/*
 This sample C++ client used gRPC asynchronous methods
 for processing the stream.
*/
#include <thread>
#include "APIClient.h"
#include <grpc++/create_channel.h>

using namespace AppTek;

APIClient::APIClient(std::string const & grpc_host) :
	_grpc_host(grpc_host)
{
	_grpc_channel =
		grpc::CreateChannel(_grpc_host, grpc::InsecureChannelCredentials());
}

APIClient::~APIClient()
{

}

// Checks channel status and forces connection attempt.
bool APIClient::IsConnected()
{
	grpc_connectivity_state channel_state = _grpc_channel->GetState(true);
	if (channel_state != GRPC_CHANNEL_READY)
	{
		_grpc_channel->WaitForConnected(std::chrono::system_clock::now() +
			std::chrono::milliseconds(5000));
		channel_state = _grpc_channel->GetState(false);
	}
	return channel_state == GRPC_CHANNEL_READY;
}

// Create a new recognition stream which
std::unique_ptr<RecognizeStream> APIClient::CreateRecognizeStream(int sample_rate)
{
	if (!IsConnected())
		return nullptr;

	std::unique_ptr<RecognizeStream> stream = 
		std::make_unique<RecognizeStream>(_grpc_channel);

	if (stream)
	{
		// Send the configuration.
		// It must be the first message transmitted
		mtp_stream::StreamRecognizeRequest request;
		request.clear_audio();
		mtp_stream::StreamRecognizeConfig* config = request.mutable_config();
		config->set_encoding(mtp_stream::StreamAudioEncoding::PCM_16bit_SI_MONO);
		config->set_sample_rate(sample_rate);
		config->set_segmentation_enabled(true);

		bool err = false;
		if (stream->CanWrite(err) && !err)
		{
			stream->_stream->Write(request, nullptr);
			return stream;
		}
	}
	return nullptr;
}

// Our recognizer stream class which maintains the state for the underlying gRPC stream
RecognizeStream::RecognizeStream(
	std::shared_ptr<grpc::Channel> & grpc_channel)
{
	_context = std::make_unique<grpc::ClientContext>();
	_service = mtp_stream::MtpStreamAsr::NewStub(grpc_channel);	
	_write_pending = true;
	// Use asynchronous gRPC stream so we don't block on reads
	_stream = _service->AsyncStreamAudio(_context.get(), &_grpc_cq, nullptr);
	// setup read
	_read_pending = true;
	_stream->Read(&_next_response, &_next_response);
}

RecognizeStream::~RecognizeStream()
{

}

// Check if there is a write or read completion event
//
// Returns false if there is no write in progress indicating
// we can send an audio packet
bool RecognizeStream::CanWrite(bool & error)
{
	void* tag;
	bool ok = true;
	bool result = false;

	if ((_write_pending || _read_pending) && _grpc_cq.Next(&tag, &ok))
	{
		if (tag == (void*)&_next_response)
		{
			if (ok)
			{
				_response = _next_response;
				_next_response.Clear();

				_read_pending = true;
				// setup next read
				_stream->Read(&_next_response, &_next_response);
			}
		}
		else if (tag == nullptr)
		{
			result = true;
			_write_pending = false;
		}
	}
	error = !ok;

	return result || !_write_pending;
}

// Send audio packet
bool RecognizeStream::Stream(std::vector<char>audio_data, size_t length)
{
	// we can't send another packet while
	// a previous write is still pending
	bool result = !_write_pending;
	
	if (!_write_pending && length)
	{
		mtp_stream::StreamRecognizeRequest request;
		std::string * data = request.mutable_audio();
		data->append(audio_data.data(), length);
		_write_pending = true;
		_stream->Write(request, nullptr);
	}

	return result;
}

// Check if response has been received
bool RecognizeStream::Response(mtp_stream::StreamRecognizeResponse& response, bool & error)
{
	if (_response.oneof_response_case() ==
		mtp_stream::StreamRecognizeResponse::OneofResponseCase::ONEOF_RESPONSE_NOT_SET &&
		_read_pending &&
		_writes_done)
	{
		// check completion queue
		CanWrite(error);
	}
	if (_response.oneof_response_case() != 
		mtp_stream::StreamRecognizeResponse::OneofResponseCase::ONEOF_RESPONSE_NOT_SET)
	{
		response = std::move(_response);
		_response.Clear();
		return true;
	}
	return false;
}

// Tell gRPC we are done sending audio data
bool RecognizeStream::WritesDone(bool & error)
{
	if (CanWrite(error) && !error)
	{
		_stream->WritesDone(nullptr);
		_writes_done = true;
		return true;
	}

	return false;
}
