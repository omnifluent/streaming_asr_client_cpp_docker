syntax = "proto3";

package mtp_stream;

service MtpStreamAsr {
	rpc CheckStatus(Empty) returns (HealthCheck) {}
	rpc StreamAudio(stream StreamRecognizeRequest) returns (stream StreamRecognizeResponse) {}
}

message Empty {
}

message HealthCheck {
	enum HealthCheckCode {
		INITIALIZING = 0;
		READY = 1;
		BUSY = 2;	
	}

	HealthCheckCode code = 1;
}

enum StreamAudioEncoding {
	PCM_16bit_SI_MONO = 0;
	FLAC = 1;
	OGG_OPUS = 2;
}

message CustomLexiconConfig {
	string lexicon_file_path = 1;
	string statetree_file_path = 2;
	string pron2word_file_path = 3;
}

message DropAudioConfig {
	float overflow_in_seconds = 1;
	float underflow_in_seconds = 2;
}

message CustomLmConfig {
	string image_file_path = 1;
	float scale = 2;
}

message RasrConfigEntry {
	string key = 1;
	string value = 2;
}

message RasrConfigDB {
	repeated RasrConfigEntry entries = 1;
}

message CustomLmConfigV2 {
	RasrConfigDB config = 1;
}

message MaxMutableSuffixConfig {
	uint32 length = 1;
	uint32 interval = 2;
}

message StreamRecognizeConfig {
	string session_id = 1;
	StreamAudioEncoding encoding = 2;
	uint32 sample_rate = 3;
	bool segmentation_enabled = 4;
	bool speaker_change_enabled = 5;
	bool segment_end_signal_enabled = 6;	
	bool confidence_enabled = 7;
	CustomLexiconConfig custom_lexicon_config = 8; // custom dictionary is disabled when it is null	
	DropAudioConfig drop_audio = 9;
	CustomLmConfig custom_lm_config = 10;
	CustomLmConfigV2 custom_lm_config_v2 = 13;
	MaxMutableSuffixConfig max_mutable_suffix_config = 11;
	bool stable_prefix = 12;
}

message StreamRecognizeRequest {
	oneof oneof_request {
		StreamRecognizeConfig config = 1;
		bytes audio = 2;
	}
}

message SegmentEnd {
	float segment_end_time = 1;
}

message StreamRecognizeTranscription {
	string transcription = 1;
}

message ProgressStatus {
	float audio_decoder_time = 1;
	float segmenter_progress_time = 2;
	float recognizer_progress_time = 3;
}

message DropAudioStarted {
	float start_time = 1;
}

message DropAudioEnded {
	float start_time = 1;
	float end_time = 2;
}

message StreamRecognizeResponse {
	oneof oneof_response {
		StreamRecognizeTranscription transcription = 1;
		SegmentEnd segment_end = 2;
		ProgressStatus progress_status = 4;
		DropAudioStarted drop_audio_started = 5;
		DropAudioEnded drop_audio_ended = 6;
	}
}
