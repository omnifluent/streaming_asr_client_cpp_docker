
ifeq "$(PLATFORM)" ""
PLATFORM=linux
endif

ifeq "$(CPU)" ""
CPU=$(shell uname --machine)
endif

ifeq "$(CPU)" "x86_64"
CPUFLAGS=-m64
VCPKG_CPU_PREFIX=x64
else
CPUFLAGS=-m32
VCPKG_CPU_PREFIX=x86
endif

VCPKG_DIR = $(HOME)/vcpkg/installed/$(VCPKG_CPU_PREFIX)-linux
SRC_DIR = .
LIB_DIR = $(SRC_DIR)/lib -L$(VCPKG_DIR)/lib
INC = $(VCPKG_DIR)/include -I$(SRC_DIR)/proto/linux 
LIBS = -lcurl -lgrpc++ -lgrpc -lgrpc_upbdefs -lprotobuf -lgpr -lssl -lre2 -lupb_textformat -lupb_reflection -lupb -lutf8_range -lcares -lz -laddress_sorting -lcrypto -labsl 


CC	= gcc
CPP	= gcc
COMMON = -I$(INC) $(CPUFLAGS) -D__USE_GNU -Wall -fPIC -fexceptions -std=c++14
ifneq "$(PROFILE)" ""
COMMON := ${COMMON}
endif

REL_FLAGS = $(COMMON) -O -ggdb
DBG_FLAGS = $(COMMON) -D_DEBUG -ggdb -O0
ifeq "$(CFG)" "REL"
FLAGS = $(REL_FLAGS)
else
CFG=DBG
FLAGS = $(DBG_FLAGS)
endif
INT_DIR = $(SRC_DIR)/$(CFG)$(CPU)

:

%.o:	%.cpp
	$(CPP) -c $(FLAGS) -x c++ $< -o $@

$(INT_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CPP) -c $(FLAGS) -x c++ $< -o $@

$(INT_DIR)/%.o: $(SRC_DIR)/%.cc
	$(CPP) -c $(FLAGS) -x c++ $< -o $@

$(INT_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) -c $(FLAGS) $< -o $@

VPATH = $(SRC_DIR):$(SRC_DIR)/proto/linux/proto
	
all: dirs \
	$(SRC_DIR)/lib/libabsl.a \
	proto \
	$(INT_DIR)/streaming_asr_client_cpp_docker

$(SRC_DIR)/proto/linux/apptek.pb.cc: $(SRC_DIR)/proto/apptek.proto
	$(VCPKG_DIR)/tools/protobuf/protoc --cpp_out=$(SRC_DIR)/proto/linux $(SRC_DIR)/proto/apptek.proto

$(SRC_DIR)/proto/linux/apptek.grpc.pb.cc: $(SRC_DIR)/proto/apptek.proto
	$(VCPKG_DIR)/tools/protobuf/protoc --grpc_out=$(SRC_DIR)/proto/linux --plugin=protoc-gen-grpc=$(VCPKG_DIR)/tools/grpc/grpc_cpp_plugin $(SRC_DIR)/proto/apptek.proto

proto: $(SRC_DIR)/proto/linux/proto/apptek.pb.cc \
	$(SRC_DIR)/proto/linux/proto/apptek.grpc.pb.cc

objs = $(INT_DIR)/main.o \
	$(INT_DIR)/apptek.grpc.pb.o \
	$(INT_DIR)/apptek.pb.o \
	$(INT_DIR)/APIClient.o

$(INT_DIR)/streaming_asr_client_cpp_docker :	$(objs)
		$(CC) $(FLAGS) -L$(LIB_DIR) $(objs) $(LIBS) -lm -lpthread -ldl -lstdc++ -o $(INT_DIR)/streaming_asr_client_cpp_docker

dirs:
	@mkdir -p $(INT_DIR)
	@mkdir -p $(SRC_DIR)/lib

$(SRC_DIR)/make_absl_amal.ar:
	@echo "CREATE $(SRC_DIR)/lib/libabsl.a" >> $@
	find $(VCPKG_DIR)/lib/ -name "libabsl_*.a"  -exec echo ADDLIB {} \; >> $@
	@echo "SAVE" >> $@

$(SRC_DIR)/lib/libabsl.a : $(SRC_DIR)/make_absl_amal.ar
	ar -M < $(SRC_DIR)/make_absl_amal.ar

clean:
	@rm -f $(objs)

realclean:
	@rm -f $(INT_DIR)/streaming_asr_client_cpp_docker
	@rm -Rf $(INT_DIR)
