// streaming_asr_client_c++_cloud.cpp
// Demonstrates how to use the AppTek Streaming API from C++
// Requires:
//  gRPC
//  protobuf
//  curl
//  jsoncpp
//  cpp-base64
//
#include <string>
#include <iostream>
#include <fstream>
#include <thread>

#include "APIClient.h"

using namespace std;
using namespace AppTek;

string _GrpcHost = "";
string _AudioFilename = "";
int _AudioSampleRate = 16000;

#define option_value(val,index) do { \
    if (++index < argc) { \
        val = argv[index]; \
    } else { \
        std::cerr << "Missing value for \"" << argv[i] << "\" option\n"; \
        return -1; \
    } }while(0)

int get_command_line(int argc, char* argv[])
{
    std::string tmp;
    for (int i = 0; i < argc; i++)
    {
        if (strcmp(argv[i], "-f") == 0)
            option_value(_AudioFilename, i);
        else if (strcmp(argv[i], "-h") == 0)
            option_value(_GrpcHost, i);
        else if (strcmp(argv[i], "-r") == 0)
        {
            option_value(tmp, i);
            int rate = atoi(tmp.c_str());
            if (rate != 16000 && rate != 8000)
            {
                std::cerr << "Invalid audio sample rate " << tmp << "\n";
                return -1;
            }
        }
    }
    return 0;
}

// Output transcription results to console
void OutputRecognitionResults(mtp_stream::StreamRecognizeResponse& response)
{
    if (response.has_transcription())
    {
        cout << "Partial: ";
        const mtp_stream::StreamRecognizeTranscription& transcription = response.transcription();
        cout << transcription.transcription()
            << "\n";
    }
}

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        cerr << "Insufficient command line options\n";
        return -1;
    }

    if (get_command_line(argc - 1, &argv[1]))
        return -1;

    if (_GrpcHost.length())
    {
        // Create our client object
        APIClient apiClient(_GrpcHost);

        // if audio filename is present we will perform a recognition
        if (_AudioFilename.length())
        {
            // send 250 msec buffers
            std::vector<char>audio_data(_AudioSampleRate * 2 / 4);
            // audio file is required to be RAW 16bit PCM in matching sample rate
            std::ifstream audioFile;
            audioFile.open(_AudioFilename, std::ios_base::in | std::ios_base::binary);
            if (audioFile)
            {
                std::unique_ptr<RecognizeStream> recognizeStream =
                    apiClient.CreateRecognizeStream(_AudioSampleRate);
                if (!recognizeStream)
                {
                    cerr << "Error creating recognizer stream\n";
                    return 0;
                }
                bool ok = true, err = false;
                int delay = 0;
                mtp_stream::StreamRecognizeResponse response;
                do
                {
                    // simulate realtime by pausing for the 
                    // duration of the last audio packet sent
                    if (delay)
                        std::this_thread::sleep_for(std::chrono::milliseconds(delay));

                    // check if we can send an audio packet
                    if (recognizeStream->CanWrite(err))
                    {
                        size_t read = 0;
                        audioFile.read(audio_data.data(), audio_data.size());
                        read = audioFile.gcount();
                        if (read)
                        {
                            ok = recognizeStream->Stream(audio_data, read);
                            if (ok)
                                delay = read / ((_AudioSampleRate / 1000) * 2);
                        }
                        else
                            audioFile.close();
                    }

                    // check if a response was received
                    if (!err && recognizeStream->Response(response, err))
                        OutputRecognitionResults(response);

                } while (audioFile && !err);
                
                audioFile.close();

                // We won't be writing any more audio so try to
                // close our write channel.
                // If this returns false, it means there are still
                // writes pending
                while (!err && !recognizeStream->WritesDone(err))
                {
                    if (recognizeStream->Response(response, err) && !err)
                        OutputRecognitionResults(response);
                    else
                        std::this_thread::sleep_for(std::chrono::milliseconds(100));
                }

                // read any remaining responses
                // err is set when channel is shutdown
                while (!err)
                {
                    if (recognizeStream->Response(response, err))
                        OutputRecognitionResults(response);
                    else
                        std::this_thread::sleep_for(std::chrono::milliseconds(100));
                }
            }
            else
                cerr << "Error opening audio file\n";
        }
        else
        {
       
        }
    }   

    return 0;
}