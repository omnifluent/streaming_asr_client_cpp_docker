#pragma once
#include <grpc++/channel.h>
#include "proto/apptek.pb.h"
#include "proto/apptek.grpc.pb.h"

namespace AppTek {

	class RecognizeStream;

	class APIClient
	{
		std::string _grpc_host;
		std::shared_ptr<grpc::Channel> _grpc_channel;

		APIClient() = delete;
	public:
		APIClient(std::string const& grpc_host);
		~APIClient();
		bool IsConnected();

		std::unique_ptr<RecognizeStream> CreateRecognizeStream(int sample_rate);
	};

	class RecognizeStream
	{
		friend class APIClient;
	protected:
		std::unique_ptr<grpc::ClientContext> _context;
		std::unique_ptr<mtp_stream::MtpStreamAsr::Stub> _service;
		std::unique_ptr < ::grpc::ClientAsyncReaderWriterInterface < mtp_stream::StreamRecognizeRequest , mtp_stream::StreamRecognizeResponse > >
			_stream;
		grpc::CompletionQueue _grpc_cq;
		mtp_stream::StreamRecognizeResponse _response, _next_response;
		bool _write_pending = false, _read_pending = false, _writes_done = false;

		RecognizeStream() = delete;

	public:
		RecognizeStream(std::shared_ptr<grpc::Channel>& grpc_channel);
		~RecognizeStream();

		// Call to check if we can send the next audio packet
		bool CanWrite(bool& error);
		// Send audio data and get a response message if available
		// If audio_data is not empty, sends the audio data to the gateway
		// return True if response received
		bool Stream(std::vector<char>audio_data, size_t length);// , gateway::RecognizeStreamResponse& response);

		bool Response(mtp_stream::StreamRecognizeResponse & response, bool& error);

		// err = True if failure
		// returns false if there are pending writes
		bool WritesDone(bool& error);
	};

} // namespace AppTek