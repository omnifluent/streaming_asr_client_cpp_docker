## AppTek Streaming ASR docker C++ client

This sample application demonstrates how to use the AppTek Streaming ASR docker service.  This sample code is written in C++ but other sample clients are available for Python, Java, Node and C#.

The streaming API utilizes gRPC for communicating with the ASR Service running within the docker container.

**Building the sample code**
The project includes both Visual Studio 2019 project and gcc Makefile.
Library dependencies for this sample code:

 - gRPC and all its dependencies; protobuf, openssl, absl, etc.

 
These packages can be built and installed separately but this project utilizes [vcpkg](https://vcpkg.io/en/index.html) for easy dependencies building.
After installing vcpkg, the following command will build and install necessary dependencies.

    ./vcpkg install grpc
Building on Windows
Load the streaming_asr_client_cpp_docker.sln solution file and build the Debug configuration.

Building on Linux
Expects gcc version which supports C++14 standard and the Makefile uses some environment variables to locate the vcpkg libraries.  Modify the **VCPKG_DIR** variable to point to the correct location of the vcpkg packages.  Run make.\

**Running**
The sample application uses the following command line arguments to set options:

|Option|Description
|--|--|
| -f | Set the raw audio data file name for processing.
| -h | Set the host and port of the ASR service running within the docker container. E.g. localhost:11005
| -r | Set the audio sample rage.  E.g. 16000

## The ASR service
The ASR service is provided in a docker image and the service started automatically when the docker container is started.
Review the "AppTek Streaming ASR docker image" document for details on how to load and run the docker image.

**Processing**
The streaming service gateway provides the *StreamAudio* method which receives audio data and returns real-time transcription results.

**StreamAudio**
To start a new recognition, first create and send a *StreamRecognizeRequest* object and set the *StreamRecognizeConfig* object properties.  This configuration structure must be the first message sent to the gateway.  View the **apptek.proto** file for details on the various message structures.

    message StreamRecognizeConfig {
		string session_id = 1;
		StreamAudioEncoding encoding = 2;
		uint32 sample_rate = 3;
		bool segmentation_enabled = 4;
		bool speaker_change_enabled = 5;
		bool segment_end_signal_enabled = 6;	
		bool confidence_enabled = 7;
		CustomLexiconConfig custom_lexicon_config = 8; // custom dictionary is disabled when it is null	
		DropAudioConfig drop_audio = 9;
		CustomLmConfig custom_lm_config = 10;
		CustomLmConfigV2 custom_lm_config_v2 = 13;
		MaxMutableSuffixConfig max_mutable_suffix_config = 11;
		bool stable_prefix = 12;
	}
Currently only 16bit raw PCM audio (*PCM_16bit_SI_MONO*) is accepted by the gateway.  The audio sample rate must match that set in the *RecognizeStreamConfig*.

Once the configuration message has been sent, messages containing raw audio data are sent with the same *StreamRecognizeRequest* object with the audio data in the *audio* value.
The response from the services is contained in the *StreamRecognizeResponse* object.

    message StreamRecognizeResponse {
		oneof oneof_response {
			StreamRecognizeTranscription transcription = 1;
			SegmentEnd segment_end = 2;
			ProgressStatus progress_status = 4;
			DropAudioStarted drop_audio_started = 5;
			DropAudioEnded drop_audio_ended = 6;
		}
	}

As the message structure above shows the response from the service which can contain one or more of the following data structures:

 - transcription - The raw ASR xml output results.  The results are considered *partial results* because the output is subject to change until the end of the segment.
 - segment_end - Signals the end of segment.
 - progress_status	- Includes information about the ASR engine processing.
 - drop_audio_started - Message is sent when the service audio buffer reaches a highwater mark and old audio data is being purged.
 - drop_audio_ended - Message is sent when the service stops purging old audio data.

**Troubleshooting**
gRPC provides a tracing mechanism which will provide information about the communication between the client and the gateway.  Set the environment variable "GRPC_VERBOSITY=DEBUG" to enable detailed output.